<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report_caseinestats
 * @copyright  Astor Bizard, 2023
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

global $DB, $OUTPUT;

$modulenames = array();
$modulesbytype = array();
foreach ($DB->get_records('modules') as $module) {
    try {
        $n = $DB->count_records($module->name);
    } catch (moodle_exception $e) {
        $n = null;
    }
    if ($n) {
        $modulesbytype[] = $n;
        $modulenames[] = get_string('modulenameplural', $module->name);
    }
}
array_multisort($modulenames, $modulesbytype);

$chart = new \core\chart_pie();
$chart->set_doughnut(true);
$chart->set_title('Module types - (total number of activities and resources: ' . array_sum($modulesbytype) . ')');
$chart->set_labels($modulenames);
$chart->set_legend_options(array('position' => 'right'));
$chart->add_series(new \core\chart_series('Number of modules', $modulesbytype));
echo html_writer::div($OUTPUT->render($chart), '', array('style' => 'width:100%'));
