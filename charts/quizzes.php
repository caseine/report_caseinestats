<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report_caseinestats
 * @copyright  Astor Bizard, 2023
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

global $CFG, $DB, $OUTPUT;

require_once($CFG->dirroot . '/question/engine/bank.php');


$sql = " SELECT q.qtype, COUNT(DISTINCT qr.itemid) as nquestions
           FROM {question} q
           JOIN {question_versions} qv ON q.id = qv.questionid
           JOIN {question_bank_entries} qbe ON qbe.id = qv.questionbankentryid
           JOIN {question_references} qr ON qr.questionbankentryid = qbe.id
          WHERE qr.component = 'mod_quiz'
            AND qr.questionarea = 'slot'
       GROUP BY q.qtype";

$questionsbytype = array();
foreach ($DB->get_records_sql($sql) as $questions) {
    $qtype = get_string('pluginname', 'qtype_' . question_bank::get_qtype($questions->qtype, false)->name());
    $questionsbytype[$qtype] = $questions->nquestions;
}
ksort($questionsbytype);

$chart = new \core\chart_pie();
$chart->set_doughnut(true);
$chart->set_title('Question type distribution in quiz usages - (total number of questions in quiz usages: ' . array_sum($questionsbytype) . ')');
$chart->set_labels(array_keys($questionsbytype));
$chart->set_legend_options(array('position' => 'right'));
$chart->add_series(new \core\chart_series('Number of questions', array_values($questionsbytype)));
echo html_writer::div($OUTPUT->render($chart), '', array('style' => 'width:80%'));

$quizbybehaviour = array();
foreach ($DB->get_records('quiz') as $quiz) {
    $qbehaviour = get_string('pluginname', 'qbehaviour_' . $quiz->preferredbehaviour);
    if (!isset($quizbybehaviour[$qbehaviour])) {
        $quizbybehaviour[$qbehaviour] = 0;
    }
    $quizbybehaviour[$qbehaviour]++;
}
ksort($quizbybehaviour);

$chart = new \core\chart_pie();
$chart->set_doughnut(true);
$chart->set_title('Question behaviour distribution in quizzes - (total number of quizzes: ' . array_sum($quizbybehaviour) . ')');
$chart->set_labels(array_keys($quizbybehaviour));
$chart->set_legend_options(array('position' => 'right'));
$chart->add_series(new \core\chart_series('Number of quizzes', array_values($quizbybehaviour)));
echo html_writer::div($OUTPUT->render($chart), '', array('style' => 'width:80%'));
