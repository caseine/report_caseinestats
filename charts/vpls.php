<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report_caseinestats
 * @copyright  Astor Bizard, 2023
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

global $CFG, $DB, $OUTPUT;

require_once($CFG->dirroot . '/mod/vpl/vpl_submission.class.php');
require_once($CFG->dirroot . '/mod/vpl/vpl.class.php');

\core\session\manager::write_close();
raise_memory_limit(MEMORY_HUGE);

if (has_capability('report/caseinestats:resetcache', context_system::instance())) {
    echo html_writer::div(html_writer::link('/report/caseinestats/resetcache.php?category=vpls', 'Reset stored data', array('class' => 'text-danger')), 'float-right');
}

$now = usergetmidnight(time());

$since = optional_param('since', $now - 86400*365, PARAM_INT);
$until = optional_param('until', 0, PARAM_INT);

$urlbase = '/report/caseinestats/index.php?category=vpls';
$options = array (
        $urlbase . '&since=0' => 'All time',
        $urlbase . '&since=' . $now => 'Today',
        $urlbase . '&since=' . ($now - 86400*365) => 'Last year'
);
for ($yearsago = 2; $yearsago <= 5; $yearsago ++) {
    $options[$urlbase . '&since=' . ($now - 86400*365*$yearsago)] = 'Last ' . $yearsago . ' years';
}
$yearstart = 1409544000; // September 1st, 2014.
$year = 2014;
while ($yearstart < $now) {
    $nextyearstart = strtotime('+1 year', $yearstart);
    $options[$urlbase . '&since=' . $yearstart . '&until=' . $nextyearstart] = $year . '-' . ($year+1);
    $yearstart = $nextyearstart;
    $year ++;
}
echo $OUTPUT->url_select(
        $options,
        $urlbase . '&since=' . $since . ($until ? '&until=' . $until : ''),
        null
);

echo html_writer::start_div('p-2 m-3', array('style' => 'border: 1px solid #bbb;background: #eee;'));
echo html_writer::div('Index', 'text-center', array('style' => 'color: #666;font-size: 1.1em;font-variant: small-caps;'));
echo html_writer::alist(array(
        html_writer::link('#reqfiles', 'Language distribution among required files', array('onclick' => 'document.getElementById("reqfiles").scrollIntoView({block:"center",behavior:"smooth",inline:"center"});return false;')),
        html_writer::link('#reqfilesbyvpl', 'Language distribution among required files by VPL', array('onclick' => 'document.getElementById("reqfilesbyvpl").scrollIntoView({block:"center",behavior:"smooth",inline:"center"});return false;')),
        html_writer::link('#subfiles', 'Language distribution among submitted files', array('onclick' => 'document.getElementById("subfiles").scrollIntoView({block:"center",behavior:"smooth",inline:"center"});return false;')),
        html_writer::link('#submissions', 'Language distribution among submissions', array('onclick' => 'document.getElementById("submissions").scrollIntoView({block:"center",behavior:"smooth",inline:"center"});return false;')),
        html_writer::link('#submissionsovertime', 'Submissions number and languages over time', array('onclick' => 'document.getElementById("submissionsovertime").scrollIntoView({block:"center",behavior:"smooth",inline:"center"});return false;')),
));
echo html_writer::end_div();

function get_submissions_data_for_timespan($start, $end) {
    // Start inclusive, end exclusive
    global $DB;
    $subdata = $DB->get_field('report_caseinestats', 'data', array('category' => 'vpls', 'name' => 'subdata_' . $start . '_' . ($end-1)));
    $subfilesdata = $DB->get_field('report_caseinestats', 'data', array('category' => 'vpls', 'name' => 'subfilesdata_' . $start . '_' . ($end-1)));
    if ($subfilesdata === false || $subdata === false) {
        // Compute.
        list($_subfilesdata, $_subdata) = compute_subs(
                "datesubmitted>=$start AND datesubmitted<$end",
                optional_param('patchsize', 1000000, PARAM_INT));
        if ($subfilesdata === false) {
            $DB->insert_record('report_caseinestats', array(
                    'category' => 'vpls',
                    'name' => 'subfilesdata_' . $start . '_' . ($end-1),
                    'data' => json_encode($_subfilesdata),
                    'timeupdated' => time()
            ));
        }
        if ($subdata === false) {
            $DB->insert_record('report_caseinestats', array(
                    'category' => 'vpls',
                    'name' => 'subdata_' . $start . '_' . ($end-1),
                    'data' => json_encode($_subdata),
                    'timeupdated' => time()
            ));
        }
        return array($_subfilesdata, $_subdata);
    } else {
        return array(json_decode($subfilesdata), json_decode($subdata));
    }
}

$extensions = array (
        'ada' => 'Ada',
        'adb' => 'Ada',
        'ads' => 'Ada',
        'all' => 'All',
        'asm' => 'Asm',
        'blockly' => 'Blockly',
        'c' => 'C',
        'cc' => 'C++',
        'circ' => 'Logisim',
        'cpp' => 'C++',
        'C' => 'C++',
        'clj' => 'Clojure',
        'cs' => 'C#',
        'd' => 'D',
        'dat' => 'OPL',
        'erl' => 'Erlang',
        'f77' => 'Fortran',
        'f' => 'Fortran',
        'f90' => 'Fortran',
        'f95' => 'Fortran',
        'f03' => 'Fortran',
        'for' => 'Fortran',
        'gif' => 'Image',
        'gif.b64' => 'Image (base 64)',
        'go' => 'Go',
        'h' => 'C/C++ header',
        'htm' => 'HTML',
        'html' => 'HTML',
        'hs' => 'Haskell',
        'java' => 'Java',
        'jar' => 'JAR file',
        'jar.b64' => 'JAR file (base 64)',
        'jpeg' => 'Image',
        'jpeg.b64' => 'Image (base 64)',
        'jpg' => 'Image',
        'jpg.b64' => 'Image (base 64)',
        'js' => 'Javascript',
        'lisp' => 'Lisp',
        'lsp' => 'Lisp',
        'lua' => 'Lua',
        'm' => 'Matlab',
        'ml' => 'Caml',
        'mod' => 'OPL',
        'opl' => 'OPL',
        'p' => 'Pascal',
        'pas' => 'Pascal',
        'pl' => 'Prolog',
        'pro' => 'Prolog',
        'perl' => 'Perl',
        'prl' => 'Perl',
        'php' => 'PHP',
        'png' => 'Image',
        'png.b64' => 'Image (base 64)',
        'py' => 'Python',
        'r' => 'R',
        'R' => 'R',
        'rb' => 'Ruby',
        'scala' => 'Scala',
        'scm' => 'Scheme',
        's' => 'Mips',
        'sh' => 'Shell',
        'sql' => 'SQL',
        'ruby' => 'Ruby',
        'ts' => 'Typescript',
        'txt' => 'Text',
        'v' => 'Verilog',
        'vhd' => 'Vhdl',
        'vhdl' => 'Vhdl',
        'xls' => 'Excel',
        'xlsx' => 'Excel',
        'xls.b64' => 'Excel (base 64)',
        'xlsx.b64' => 'Excel (base 64)',
);

$otherkey = 'Other';
$unknownextensions = array();

function compute_subs($select = "", $patchsize = 0) {
    global $DB, $CFG, $extensions, $unknownextensions, $otherkey;
    $filesbylanguage = array();
    $submissionsbylanguage = array();
    if (!isset($filesbylanguage[$otherkey])) {
        $filesbylanguage[$otherkey] = 0;
        $submissionsbylanguage[$otherkey] = 0;
    }
    $i = 0;
    while (true) {
        $rc = $DB->get_recordset_select('vpl_submissions', $select, null, '', 'id, userid, vpl', $i, $patchsize);
        $i += $patchsize;
        if (!$rc->valid()) {
            $rc->close();
            break;
        }
        while ($rc->valid()) {
            $submissionrecord = $rc->current();
            $filename = $CFG->dataroot . '/vpl_data/' . $submissionrecord->vpl . '/usersdata/' . $submissionrecord->userid . '/' . $submissionrecord->id . '/submittedfiles.lst';
            if (!is_file($filename)) {
                $rc->next();
                continue;
            }
            $subfiles = explode("\n", file_get_contents($filename));
            $sublanguages = array();
            foreach ($subfiles as $subfile) {
                $subfile = trim($subfile);
                $extension = pathinfo($subfile, PATHINFO_EXTENSION);
                if ($extension == 'b64') {
                    $extension = pathinfo(substr($subfile, 0, -4), PATHINFO_EXTENSION) . '.b64';
                }
                if (isset($extensions[$extension])) {
                    $extname = $extensions[$extension];
                } else {
                    $extname = $otherkey;
                    if (!isset($unknownextensions[$extension ?: '[none]'])) {
                        $unknownextensions[$extension ?: '[none]'] = 0;
                    }
                    $unknownextensions[$extension ?: '[none]'] ++;
                }
                if (!isset($filesbylanguage[$extname])) {
                    $filesbylanguage[$extname] = 0;
                }
                $filesbylanguage[$extname]++;
                $sublanguages[$extname] = true;
            }
            foreach (array_keys($sublanguages) as $language) {
                if (!isset($submissionsbylanguage[$language])) {
                    $submissionsbylanguage[$language] = 0;
                }
                $submissionsbylanguage[$language]++;
            }
            $rc->next();
        }
        $rc->close();
    }

    return array($filesbylanguage, $submissionsbylanguage);
};

function get_color_palette($data) {
    global $otherkey;
    $palette = array(
            '#f3c300',
            '#875692',
            '#f38400',
            '#a1caf1',
            '#be0032',
            '#c2b280',
            '#7f180d',
            '#008856',
            '#e68fac',
            '#0067a5',
            '#e6194B',
            '#3cb44b',
            '#4363d8',
            '#911eb4',
            '#42d4f4',
            '#bfef45',
            '#f032e6',
            '#fabed4',
            '#469990',
            '#dcbeff',
            '#9A6324',
            '#fffac8',
            '#800000',
            '#aaffc3',
            '#808000',
            '#000075',
            '#ffd8b1',
    );
    for ($i = 0; $i < count($data) / count($palette); $i ++) {
        $palette = array_merge($palette, $palette);
    }
    $colors = array_slice($palette, 0, count($data));
    if (isset($data[$otherkey])) {
        $colors = array_slice($colors, 0, -1);
        $colors[] = '#cccccc';
    }
    return $colors;
}

function draw_chart($title, $data, $serieslegend, $attributes = array()) {
    global $OUTPUT, $otherkey;
    $chart = new \core\chart_pie();
    $chart->set_doughnut(true);
    $chart->set_title($title);
    $chart->set_labels(array_keys($data));
    $chart->set_legend_options(array('position' => 'right'));
    $series = new \core\chart_series($serieslegend, array_values($data));
    $series->set_colors(get_color_palette($data));
    $chart->add_series($series);
    echo html_writer::div($OUTPUT->render($chart), '', array_merge(array('style' => 'width:80%'), $attributes));
}

$conditions = array();
if ($since) {
    $conditions[] = "datesubmitted>=$since";
}
if ($until) {
    $conditions[] = "datesubmitted<=$until";
}

$base = 1409544000; // September 1st, 2014.
// Advance month by month until start.
while ($base < $since) {
    $base = strtotime('+1 month', $base);
}
list($filesbylanguage, $submissionsbylanguage) = compute_subs(
        "datesubmitted>=$since AND datesubmitted<$base",
        optional_param('patchsize', 1000000, PARAM_INT));

$currenttime = $base;
if (!$until || $until > time()) {
    $until = time();
}
while (strtotime('+1 month', $currenttime) < $until) {
    $nexttime = strtotime('+1 month', $currenttime);
    list($filesdata, $submissionsdata) = get_submissions_data_for_timespan($currenttime, $nexttime);
    foreach($filesdata as $language => $n){
        if(!isset($filesbylanguage[$language])){
            $filesbylanguage[$language] = 0;
        }
        $filesbylanguage[$language] += $n;
    }
    foreach($submissionsdata as $language => $n){
        if(!isset($submissionsbylanguage[$language])){
            $submissionsbylanguage[$language] = 0;
        }
        $submissionsbylanguage[$language] += $n;
    }
    $currenttime = $nexttime;
}
list($filesdata, $submissionsdata) = compute_subs(
        "datesubmitted>=$currenttime AND datesubmitted<$until",
        optional_param('patchsize', 1000000, PARAM_INT));
foreach($filesdata as $language => $n){
    if(!isset($filesbylanguage[$language])){
        $filesbylanguage[$language] = 0;
    }
    $filesbylanguage[$language] += $n;
}
foreach($submissionsdata as $language => $n){
    if(!isset($submissionsbylanguage[$language])){
        $submissionsbylanguage[$language] = 0;
    }
    $submissionsbylanguage[$language] += $n;
}

$reqfilesbylanguage = array();
$vplsbylanguage = array();
$rc = $DB->get_recordset('vpl', null, '', 'id');
while ($rc->valid()) {
    $vplrecord = $rc->current();
    $filename = $CFG->dataroot . '/vpl_data/' . $vplrecord->id . '/required_files.lst';
    if (!is_file($filename)) {
        $rc->next();
        continue;
    }
    $reqfiles = explode("\n", file_get_contents($filename));
    $vpllanguages = array();
    foreach ($reqfiles as $reqfile) {
        $reqfile = trim($reqfile);
        $extension = pathinfo($reqfile, PATHINFO_EXTENSION);
        if ($extension == 'b64') {
            $extension = pathinfo(substr($reqfile, 0, -4), PATHINFO_EXTENSION) . '.b64';
        }
        if (isset($extensions[$extension])) {
            $extname = $extensions[$extension];
        } else {
            $extname = $otherkey;
            if (!isset($unknownextensions[$extension ?: '[none]'])) {
                $unknownextensions[$extension ?: '[none]'] = 0;
            }
            $unknownextensions[$extension ?: '[none]'] ++;
        }
        if (!isset($reqfilesbylanguage[$extname])) {
            $reqfilesbylanguage[$extname] = 0;
        }
        $reqfilesbylanguage[$extname]++;
        $vpllanguages[$extname] = true;
    }
    foreach (array_keys($vpllanguages) as $language) {
        if (!isset($vplsbylanguage[$language])) {
            $vplsbylanguage[$language] = 0;
        }
        $vplsbylanguage[$language]++;
    }
    $rc->next();
}
$rc->close();

$indexedlanguages = array_keys(array_merge($vplsbylanguage, $submissionsbylanguage));
$filler = array_fill_keys($indexedlanguages, 0);

$reqfilesbylanguage = array_merge($filler, $reqfilesbylanguage);
ksort($reqfilesbylanguage);
$other = $reqfilesbylanguage[$otherkey];
unset($reqfilesbylanguage[$otherkey]);
$reqfilesbylanguage[$otherkey] = $other;
draw_chart(
        'Language distribution among required files - (total number of required files: ' . array_sum($reqfilesbylanguage) . ')',
        $reqfilesbylanguage,
        'Number of required files',
        array('id' => 'reqfiles')
        );

$vplsbylanguage = array_merge($filler, $vplsbylanguage);
ksort($vplsbylanguage);
$other = $vplsbylanguage[$otherkey];
unset($vplsbylanguage[$otherkey]);
$vplsbylanguage[$otherkey] = $other;
draw_chart(
        'Language distribution among required files by VPL - (total number of VPL: ' . $DB->count_records('vpl') . ')',
        $vplsbylanguage,
        'Number of VPLs',
        array('id' => 'reqfilesbyvpl')
        );

$filesbylanguage = array_merge($filler, $filesbylanguage);
ksort($filesbylanguage);
$other = $filesbylanguage[$otherkey];
unset($filesbylanguage[$otherkey]);
$filesbylanguage[$otherkey] = $other;
draw_chart(
        'Language distribution among submitted files - (total number of submitted files: ' . array_sum($filesbylanguage) . ')',
        $filesbylanguage,
        'Number of files',
        array('id' => 'subfiles')
        );

$submissionsbylanguage = array_merge($filler, $submissionsbylanguage);
ksort($submissionsbylanguage);
$other = $submissionsbylanguage[$otherkey];
unset($submissionsbylanguage[$otherkey]);
$submissionsbylanguage[$otherkey] = $other;
draw_chart(
        'Language distribution among submissions - (total number of submissions: ' . $DB->count_records_select('vpl_submissions', implode(" AND ", $conditions)) . ')',
        $submissionsbylanguage,
        'Number of submissions',
        array('id' => 'submissions')
        );

arsort($unknownextensions);
echo '<details><summary>Extensions under "Other"</summary>' . implode(', ', array_map(function($ext, $n) {return $ext . ' (' . $n . ')';}, array_keys($unknownextensions), $unknownextensions)) . '</details>';

$series = array();
$dates = array();
$i = 0;
$currenttime = 1409544000; // September 1st, 2014.
while (strtotime('+1 month', $currenttime) < time()) {
    $nexttime = strtotime('+1 month', $currenttime);
    $submissionsdata = get_submissions_data_for_timespan($currenttime, $nexttime)[1];
    $hassubs = false;
    foreach($submissionsdata as $language => $nsub){
        if ($nsub > 0) {
            $hassubs = true;
            if (!isset($series[$language])) {
                $series[$language] = array();
            }
            if (count($series[$language]) < $i) {
                $series[$language] += array_fill(count($series[$language]), $i - count($series[$language]), null);
            }
            $series[$language][] = $nsub;
        }
    }
    $currenttime = $nexttime;
    if ($i > 0 || $hassubs) {
        $i++;
        $dates[] = userdate($currenttime, get_string('strftimemonthyear', 'langconfig'));
    }
}

ksort($series);
$other = $series[$otherkey];
unset($series[$otherkey]);
$series[$otherkey] = $other;
$chart = new \core\chart_bar();
$chart->set_title('Submissions number and languages over time');
$chart->set_stacked(true);
$chart->set_labels($dates);
$palette = get_color_palette($series);
$j = 0;
foreach ($series as $language => &$subs) {
    if (count($subs) < $i) {
        $series[$language] += array_fill(count($series[$language]), $i - count($series[$language]), null);
    }
    $langseries = new \core\chart_series($language, array_values($subs));
    $langseries->set_color($palette[$j++]);
    $chart->add_series($langseries);
}
echo html_writer::div($OUTPUT->render($chart), '', array('id' => 'submissionsovertime'));
