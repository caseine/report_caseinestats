<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require(__DIR__ . '/../../config.php');

global $CFG, $PAGE, $OUTPUT;

require_login();

require_once($CFG->libdir . '/graphlib.php');

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($CFG->wwwroot.'/report/caseinestats/index.php'));
$PAGE->set_pagelayout('standard');
$PAGE->set_title('Caseine global statistics');
$PAGE->navbar->add('Caseine global statistics');

require_capability('report/caseinestats:view', $context);

echo $OUTPUT->header();
echo $OUTPUT->heading('Caseine global statistics');

$categories = array(
        'modules' => 'Modules',
        'vpls' => 'VPLs',
        'quizzes' => 'Quizzes',
);

$category = optional_param('category', 'modules', PARAM_ALPHANUMEXT);

if (!in_array($category, array_keys($categories))) {
    throw new invalid_parameter_exception();
}

echo $OUTPUT->tabtree(
        array_map(function($categoryid, $categoryname) {
            return new tabobject($categoryid, new moodle_url('/report/caseinestats/index.php?category=' . $categoryid), $categoryname);
        }, array_keys($categories), $categories),
        $category);

echo $OUTPUT->box_start();

include(__DIR__ . '/charts/' . $category . '.php');

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
