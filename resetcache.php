<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require(__DIR__ . '/../../config.php');

global $CFG, $PAGE, $DB, $OUTPUT;

require_login();

$context = context_system::instance();

$category = optional_param('category', null, PARAM_ALPHANUMEXT);

$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($CFG->wwwroot.'/report/caseinestats/resetcache.php' . ($category ? '?category=' . $category : '')));
$PAGE->set_pagelayout('standard');
$PAGE->set_title('Caseine global statistics');
$PAGE->navbar->add('Caseine global statistics');

require_capability('report/caseinestats:resetcache', $context);

$confirm = optional_param('confirm', null, PARAM_BOOL);
if ($confirm === null) {
    echo $OUTPUT->header();
    echo $OUTPUT->confirm(
            'Are you sure you want to reset all the cached data' . ($category ? ' for "' . $category . '"' : '') . '?',
            $PAGE->url->out(false, array('confirm' => 1)),
            $PAGE->url->out(false, array('confirm' => 0))
            );
    echo $OUTPUT->footer();
} else {
    if ($confirm && confirm_sesskey()) {
        $DB->delete_records('report_caseinestats', $category ? array('category' => $category) : null);
    }
    redirect('/report/caseinestats/index.php' . ($category ? '?category=' . $category : ''));
}
